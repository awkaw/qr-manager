<?php

namespace QrManager\PaymentService;

class QrCode
{
    private $sum;
    private $qr_size = 400;
    private $nomenclature = [];
    private $payment_purpose;
    private $notification_url;
    private $customer_email;
    private $redirect_url;

    public function getSum()
    {
        return $this->sum;
    }

    public function setSum($sum): QrCode
    {
        $this->sum = $sum;

        return $this;
    }

    public function getQrSize(): int
    {
        return $this->qr_size;
    }

    public function setQrSize($qr_size): QrCode
    {
        $this->qr_size = $qr_size;

        return $this;
    }

    public function getPaymentPurpose()
    {
        return $this->payment_purpose;
    }

    public function setPaymentPurpose($payment_purpose): QrCode
    {
        $this->payment_purpose = $payment_purpose;

        return $this;
    }

    public function getNotificationUrl()
    {
        return $this->notification_url;
    }

    public function setNotificationUrl($notification_url): QrCode
    {
        $this->notification_url = $notification_url;

        return $this;
    }

    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    public function setCustomerEmail($customer_email): QrCode
    {
        $this->customer_email = $customer_email;

        return $this;
    }

    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    public function setRedirectUrl($redirect_url): QrCode
    {
        $this->redirect_url = $redirect_url;

        return $this;
    }

    public function getNomenclature(): array
    {
        return $this->nomenclature;
    }

    public function addNomenclature(Nomenclature $nomenclature): QrCode
    {
        $this->nomenclature[] = [
            "name" => $nomenclature->getName(),
            "price" => $nomenclature->getPrice(),
            "count" => $nomenclature->getCount(),
            "payment_method" => $nomenclature->getPaymentMethod(),
            "payment_type" => $nomenclature->getPaymentType(),
            "nds" => $nomenclature->getNds(),
        ];

        return $this;
    }
}