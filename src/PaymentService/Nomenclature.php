<?php

namespace QrManager\PaymentService;

class Nomenclature
{
    protected $name;
    protected $count = 1;
    protected $price = 0;
    protected $nds = -1;
    protected $payment_method = 1;
    protected $measure = "PIECE";
    protected $payment_type = 4;

    public function setName($name): Nomenclature
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): Nomenclature
    {
        $this->count = $count;

        return $this;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice($price): Nomenclature
    {
        $this->price = $price;

        return $this;
    }

    public function setNds($nds): Nomenclature
    {
        $this->nds = $nds;

        return $this;
    }

    public function getNds()
    {
        return $this->nds;
    }

    public function setPaymentMethod(int $payment_method): Nomenclature
    {
        $this->payment_method = $payment_method;

        return $this;
    }

    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    public function setMeasure(string $measure): Nomenclature
    {
        $this->measure = $measure;

        return $this;
    }

    public function getMeasure()
    {
        return $this->measure;
    }

    public function setPaymentType(int $payment_type): Nomenclature
    {
        $this->payment_type = $payment_type;

        return $this;
    }

    public function getPaymentType()
    {
        return $this->payment_type;
    }
}