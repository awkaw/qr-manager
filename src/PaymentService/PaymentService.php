<?php

namespace QrManager\PaymentService;

use QrManager\Helpers\ConfigHelper;

class PaymentService
{
    private array $config;
    protected string $apiUrl = "https://app.wapiserv.qrm.ooo";

    public function __construct()
    {
        $this->config = [
            "apiKey" => ConfigHelper::qrManagerConfig("api_key"),
            "apiDev" => ConfigHelper::qrManagerConfig("api_dev"),
            "apiLogin" => ConfigHelper::qrManagerConfig("api_login"),
        ];
    }

    public static function getInstance(): PaymentService
    {
        return new self();
    }

    public function setApiLogin($login): void
    {
        $this->config["apiLogin"] = $login;
    }

    public function setApiKey($key): void
    {
        $this->config["apiKey"] = $key;
    }

    public function setApiUrl($url): void
    {
        $this->apiUrl = $url;
    }

    private function getPostDataFromQrCode(QrCode $qrCode): array
    {
        $postData = [
            "sum" => $qrCode->getSum()
        ];

        if(!is_null($qrCode->getQrSize())){
            $postData['qr_size'] = $qrCode->getQrSize();
        }

        if(!is_null($qrCode->getPaymentPurpose())){
            $postData['payment_purpose'] = $qrCode->getPaymentPurpose();
        }

        if(!is_null($qrCode->getNotificationUrl())){
            $postData['notification_url'] = $qrCode->getNotificationUrl();
        }

        if(!is_null($qrCode->getCustomerEmail())){
            $postData['customer_email'] = $qrCode->getCustomerEmail();
        }

        if(!is_null($qrCode->getRedirectUrl())){
            $postData['redirect_url'] = $qrCode->getRedirectUrl();
        }

        if(!empty($qrCode->getNomenclature())){
            $postData['nomenclature'] = $qrCode->getNomenclature();
        }

        return $postData;
    }
    
    public function getQrCode(QrCode $qrCode)
    {
        $url = $this->apiUrl."/operations/qr-code/";

        $headers = [
            "X-Api-Key" => $this->config['apiKey'],
            "Content-Type" => "application/json",
        ];

        if(!$this->config['apiDev']){

            $headers["X-Api-Login"] = $this->config['apiLogin'];
        }

        $response = \QrManager\Http\Requests\Request::getInstance()->post($url, $this->getPostDataFromQrCode($qrCode), $headers);

        return $response->getBody()->getContents();
    }

    public function getQrCodeWithNomenclature(QrCode $qrCode): string
    {
        $url = $this->apiUrl."/operations/qr-code/";

        $sum = 0;

        foreach ($qrCode->getNomenclature() as $nomenclature) {

            $sum += $nomenclature['price'] * $nomenclature['count'];
        }

        $qrCode->setSum($sum);

        $headers = [
            "X-Api-Key" => $this->config['apiKey'],
            "Content-Type" => "application/json",
        ];

        if(!$this->config['apiDev']){

            $headers["X-Api-Login"] = $this->config['apiLogin'];
        }

        $response = \QrManager\Http\Requests\Request::getInstance()->post($url, $this->getPostDataFromQrCode($qrCode), $headers);

        return $response->getBody()->getContents();
    }

    public function getStatusPayment($id): string
    {
        $url = $this->apiUrl."/operations/{$id}/qr-status/";

        $headers = [
            "X-Api-Key" => $this->config['apiKey'],
        ];

        $response = \QrManager\Http\Requests\Request::getInstance()->get($url, null, $headers);

        return $response->getBody()->getContents();
    }
}