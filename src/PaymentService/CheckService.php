<?php

namespace QrManager\PaymentService;

use QrManager\Helpers\ConfigHelper;

class CheckService
{
    private array $config;

    public function __construct()
    {
        $this->config = [
            "api_key" => ConfigHelper::qrManagerConfig("api_key"),
            "api_dev" => ConfigHelper::qrManagerConfig("api_dev"),
            "api_login" => ConfigHelper::qrManagerConfig("api_login"),
        ];
    }

    public function registerCheck()
    {

    }
}