<?php

namespace QrManager\Http\Requests;

use GuzzleHttp\Client;

class Request
{
    public static function getInstance(): Request
    {
        return new self();
    }

    public function get($url, $data, $headers = []): \Psr\Http\Message\ResponseInterface
    {
        return $this->request($url, $data, $headers, "get");
    }

    public function post($url, $data, $headers = []): \Psr\Http\Message\ResponseInterface
    {
        return $this->request($url, $data, $headers, "post");
    }

    private function request($url, $data, $headers = [], $type = "post"): \Psr\Http\Message\ResponseInterface
    {
        if(is_array($data)){

            $data = json_encode($data);
        }

        $client = new Client();

        $request = new \GuzzleHttp\Psr7\Request($type, $url, $headers, $data);

        return $client->send($request);
    }
}