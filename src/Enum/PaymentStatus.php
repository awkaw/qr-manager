<?php

namespace QrManager\Enum;

class PaymentStatus
{
    const CREATED = 3;
    const PROCESS = 4;
    const SUCCESS = 5;
    const CANCEL = 6;
    const EXPIRED = 8;
    const TIMEOUT = 0;
}