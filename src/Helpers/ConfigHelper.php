<?php

namespace QrManager\Helpers;

class ConfigHelper{

    public static function qrManagerConfig($param)
    {
        $config = require(__DIR__."./../config.php");

        if(isset($config[$param])){

            return $config[$param];
        }

        return null;
    }
}