<?php

return [

    "api_login" => getenv("QR_MANAGER_LOGIN"),

    "api_key" => getenv("QR_MANAGER_KEY"),

    "api_dev" => false,
];